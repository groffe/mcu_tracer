#define _POSIX_THREADS 1

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
//#include <alloca.h>
#include <gtk/gtk.h>
#include <pthread.h>
//#include <semaphore.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
//#include <pwd.h>
#include "interfacer.h"
#include "connect.h"
#include "rs232.h"
#include "debugwindow.h"
#include "settingssaver.h"


extern GMutex *lock_rs232;
extern GMutex *lock_paritycheckbit;
extern GMutex *lock_rec_thread;


#define BAUDRATES_COUNT 30
#if defined(__linux__) || defined(__FreeBSD__)   /* Linux & FreeBSD */
	#define COMPORT_COUNT 39
	#define COMPORT_ENTRY_LEN 16
	#include <pwd.h>
#else 											/*Windows*/
	#define COMPORT_COUNT 16
	#define COMPORT_ENTRY_LEN 10
#endif
